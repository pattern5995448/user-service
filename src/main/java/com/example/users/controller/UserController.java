package com.example.users.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user-service")
public class UserController {
	
	@Value("${spring.application.name}")
	String serviceName;
	
	@GetMapping("health-check")
	public String status(HttpServletRequest request) {
		return String.format("%s Connected : %s port", serviceName, request.getServerPort() );
	}
	

}
